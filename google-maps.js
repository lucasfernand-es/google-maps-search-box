// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.
// Autocomplete is used in FillAddress()
var autocomplete;

// Initial place for the maps
var newYorkID = 'ChIJOwg_06VPwokRYv534QaPC8g';


/*
  The next add will be choose the zoom depending on the kind of the search 
*/
// Depending on the kind of place

// Determine the initial zoom
var zoomInitial = 13; // Because 13 looks great - Cities Names

// The zoom that will be used when the map is focused
var zoomFocus = 15;  // Why 16? Because it looks good.  - Neighborhoods names

// For more information about address components, visit: https://developers.google.com/maps/documentation/geocoding/#Types
var componentForm = {
  pac_input: 'long_name',
  name: 'long_name', // Name is NOT one of the properties of address components
  street_address: 'long_name',
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

function initializeMap(mapName, locationID, zoom){

  /* Random Map */
  var map = new google.maps.Map(document.getElementById(mapName), {
    //center: new google.maps.LatLng(-33.8665433, 151.1956316),
    zoom: zoom
  });

  var request = {
    placeId: locationID
  };

  var infowindow = new google.maps.InfoWindow();
  var service = new google.maps.places.PlacesService(map);

  // Whichever information that need to be retrived from the place, it must be done inside the following code
  service.getDetails(request, function(place, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {

      map.setCenter(place.geometry.location);

      var marker = new google.maps.Marker({
        position: place.geometry.location,
        map: map
      });

      setAddress(place);
      
      google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(place.name);
        infowindow.open(map, this);
      });

    }
  });

  return map;
}

function setAddress(place){
  

  var address = [];
  var name;

  for (i = 0; i < place.address_components.length; i++) 
  {
    addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      val = place.address_components[i][componentForm[addressType]];
      if(addressType == 'route')
        address.push(val);
      else if(addressType == 'street_number')
        address.push(val);
    }
  }
  var fullAddress = address.join(' ');
  if(fullAddress == place.name)
    name = '';
  else
    name = place.name;

  document.getElementById('place_name').textContent = name;
  document.getElementById('full_address').textContent = place.formatted_address;

  // Set the link to go to Google Maps. Search the location based on where the user is.
  document.getElementById("directions").href='http://maps.google.com/?saddr=Current+Location&daddr=' + place.geometry.location; 
}

function initialize() {

  var map;
  /* This is the configuration of the outer map  */
  // initializeMap creates a map -- does not need to store this map information
  initializeMap('map-location', newYorkID, zoomInitial);

  /*  This is the configuration of the inner map  */
  map = initializeMap('map-search', newYorkID, zoomInitial);


 input = /** @type {HTMLInputElement} */(
      document.getElementById('pac_input'));
  
  var types = document.getElementById('type-selector');

  autocomplete = new google.maps.places.Autocomplete(input);
  autocomplete.bindTo('bounds', map);

  google.maps.event.addListener(autocomplete, 'place_changed', function() 
  {
    // Make the map visible
    showElement('search-details');
  
    //Clear everything
    clear();
    // When the user selects an address from the dropdown,
    // populate the address fields in the form.
    fillInAddress();
    
    var place = autocomplete.getPlace();

    if (!place.geometry) {
      window.alert("It was not possible to find.");
      return;
    }
    // Defining new place for maps
    initializeMap('map-search', place.place_id, zoomFocus);
    initializeMap('map-location', place.place_id, zoomFocus);

  

    var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''),
        (place.address_components[1] && place.address_components[1].short_name || ''),
        (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }

  });

  // Sets a listener on a radio button to change the filter type on Places
  // Autocomplete.
  function setupClickListener(id, types) {
    var radioButton = document.getElementById(id);
    google.maps.event.addDomListener(radioButton, 'click', function() {
      autocomplete.setTypes(types);
    });
  }

  setupClickListener('changetype-all', []);
  setupClickListener('changetype-address', ['address']);
  setupClickListener('changetype-establishment', ['establishment']);
  setupClickListener('changetype-geocode', ['geocode']);
}

// This function clean all the data
function clear(){
  // Refresh the inner map
  initializeMap('map-search', newYorkID, zoomInitial);
  // Refresh the outer map
  initializeMap('map-location', newYorkID, zoomInitial);
  
  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }
}

// [START region_fillform]
// The address model is following the american system
function fillInAddress() {

  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  
  var address = [];

  for (i = 0; i < place.address_components.length; i++) {
    addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      val = place.address_components[i][componentForm[addressType]];
      if(addressType == 'route'){
        address.push(val);
        //route = val;
      }
      else if(addressType == 'street_number'){
        address.push(val);
        //street_number = val + ' ';
      }
      
      document.getElementById(addressType).value = val;
    }
    var fullAddress = address.join(' ');
    // Fill the Full Address. Property street_address not working
    //fullAddress = street_number + route;
    if(fullAddress == place.name)
      // Fill the component name
      document.getElementById('name').value = '';
    else
      document.getElementById('name').value = place.name;

    document.getElementById('street_address').value = fullAddress;

    // Show the search details and hide the search input field
    //hideElement('search');
  }
}
// [END region_fillform]

function showElement(name){
  document.getElementById(name).style.display = "inline";
}

function hideElement(name){
  document.getElementById(name).style.display = "none";
}

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
if (navigator.geolocation) {
  navigator.geolocation.getCurrentPosition(function(position) {
    var geolocation = new google.maps.LatLng(
        position.coords.latitude, position.coords.longitude);
    var circle = new google.maps.Circle({
      center: geolocation,
      radius: position.coords.accuracy
    });
    autocomplete.setBounds(circle.getBounds());
  });
}
}
// [END region_geolocation]
function refresh() {
clear();
// Show the search details and hide the search input field
showElement('search');
//hideElement('search-details');
}

google.maps.event.addDomListener(window, 'load', initialize);